package logica;
/*
* Trabalhando com arrays
* Criação
* Acesso a elementos
* Classe arrays
* Pesquisa
* Ordenação
*/

//Importando classe de manipulação de arrays para exibir conteudo de array na tela.
import java.util.Arrays;

public class Array{

	public static void main(String[] args) {
		
		String[] paises = {"Brasil", "japan", "Irlanda", "Canada"};
		System.out.println(paises[0]);

		/*Descobrindo quantas posições tem um array.*/
		System.out.println(paises.length);

		//Transformando array em string para exibir o conteúdo de um array. Tipo print_r do php.
		System.out.println(Arrays.toString(paises));

		/*pesquisando dentro do array.
		* utiliza dois parametros
		*/
		int posicao = Arrays.binarySearch(paises, "japan");
		System.out.println(posicao);

		/*Ordenando array com metodo sort.
		*Arrays.sort(nomedoarray, 
		*posicao inicial que é 0(zero), 
		*posicao final (utiliza-se o array.lenght para pegar o tamanho do array.));
		*/
		Arrays.sort(paises, 0, paises.length);
		System.out.println(Arrays.toString(paises));

		//Acessando os metodos do objeto dentro do array.
		Double[] valores = {12.34, 2343.5454};
		System.out.println(valores[0].doubleValue());


		// Array multidimensional

		String[] uma = {"Mateus", "Jussara", "Maria Luiza"};
		String[][] duas = 
		{
			{"Mateus", "M", "MG"},
			{"Jussara", "F", "MG"}
		};

		/*acessando o primeiro elemento do array de duas dimensões.
		* O primeiro Colchete é o indice do array interno
		* O segundo colchete é o array mais externo
		*/
		System.out.println(uma[0]);
		System.out.println(duas[0][0]);
		System.out.println(duas[1][0]);

		
	}
}