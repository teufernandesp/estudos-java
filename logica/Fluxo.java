package logica;
import java.util.ArrayList;

//FOREACH JAVA
public class  Fluxo{

	public static void main(String[] args) {
		
		ArrayList<Integer> list = new ArrayList<>();

		for (int i=0; i<10 ; i++) {
			
			list.add(i);
		}

		for (Integer numero : list ) {
			
			System.out.println(numero);
		}

	}
}