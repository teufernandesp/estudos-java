package logica;

//trabalhando com colection Framework.

//mportando a classe ArrayList.
import java.util.ArrayList;

public class ArrayListTest{

	public static void main(String[] args) {
	
	//Conjunto de classes que forma um framework.

		ArrayList <String> cores = new ArrayList <>();
		cores.add("Branco");
		cores.add("Verde");
		cores.add("Azul");
		cores.add(0, "Preto"); //definindo o indice do elemento.

		//imprimindo o array em forma de texto.
		System.out.println(cores.toString());

		//Pegando o tamanho do arraylist com o size.
		System.out.println("Tamanho: " + cores.size());

		//recuperando elementos dentro do arraylist. Metodo Get.
		System.out.println("Elemento 2: " + cores.get(2));

		//recuperando o indice do elemento.
		System.out.println("Indice Branco: " + cores.indexOf("Branco"));

		//removendo elementos dentro do arraylist.
		cores.remove("Branco");

		boolean verificaCores =  cores.contains("Verde");
		
		System.out.println(verificaCores);
	}
}