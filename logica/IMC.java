package logica;
/* Calcular o indice de massa corporal
* IMC = pesoEmQuilogramas / alturaEmMetros * alturaEmMetros
*/

import javax.swing.JOptionPane;

public class IMC{

	public static void main(String[] args) {
			
		String peso = JOptionPane.showInputDialog("Informe seu Peso");
		String altura = JOptionPane.showInputDialog("Informe sua altura");

		double pesoEmQuilogramas = Double.parseDouble(peso);
		double alturaEmMetros = Double.parseDouble(altura);

		double imc = pesoEmQuilogramas / (alturaEmMetros * alturaEmMetros);

		String msg = (imc >= 20 && imc <= 25) ? "Peso Ideal": "Acima do Peso";
		JOptionPane.showMessageDialog(null, "Voce es" + msg + "Seu IMC e: " + imc);
	}
}