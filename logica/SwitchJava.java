package logica;
import javax.swing.JOptionPane;

public class SwitchJava{
	public static void main(String[] args) {
		
		
		String tecnologia = JOptionPane.showInputDialog("Informe M ou F");

		switch (tecnologia){

			case "Java":
			case "C++":
			case "Cobol":
				System.out.println("Linguagem de programação");
				break;

			case "Oracle":
			case "SQL Server":
			case "Postgresql":
				System.out.println("Banco de dados");
				break;

			default:
				System.out.println("Não informou nenhum valor");
				break;
		}

	}
}