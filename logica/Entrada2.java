package logica;
//Importando a classe JOptionPane
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Entrada2 {

	public static void main(String[] args) {
		
		//Exibir caixa de diálogo
		String nome = JOptionPane.showInputDialog("Qual o seu nome?");
		
		//Alert.
		//JOptionPane.showMessageDialog(null, "Seu Nome está errado!!!!", "alert", JOptionPane.ERROR_MESSAGE);

		//Information
		final JPanel panel = new JPanel();
		JOptionPane.showMessageDialog(panel, nome, "Question",
        JOptionPane.INFORMATION_MESSAGE);
		
		//Exibe texto em outra janela.
		//JOptionPane.showMessageDialog(null, nome);
	}
}