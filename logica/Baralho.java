package logica;
//Importando a classe random
import java.util.Random;

public class Baralho{

	public static void main(String[] args) {
		
		String[] faces = 
		{
			"A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Valete", "Dama", "Rei"
		};
		
		String[] nipes = 
		{
			"Espadas", "Paus", "Copas", "Ouros"
		};

		//escolhendo aleatoriamente com classe random.
		Random r = new Random(1);

		//Pega a o tamanho do array de faces e de nipes com o length.
		int iFace = r.nextInt(faces.length);
		int iNipe = r.nextInt(nipes.length);

		//imprime os arrays com índices aleatórios.
		System.out.println(faces[iFace] + " " + nipes[iNipe]);
	}
}