package logica;
//Importando as classes
import java.util.ArrayList;
import java.util.Scanner;

public class ProdutosFluxo{

	public static void main(String[] args) {
		
		//Criando objeto array list
		ArrayList<String> produtos = new ArrayList<>();
		
		//Criando objeto scanner para entrada de dados na tela do sistema.
		Scanner s = new Scanner(System.in);
		System.out.println("Informe os produtos e clique FIM para sair");

		//Coletando os dados inseridos pelo usuario.
		String produto;

		//Adiciona o valor a variavel produto e verifica se é igual a FIM.
		while (!"FIM".equals(produto = s.nextLine())) {
			//Adiciona o produto ao arraylist.
			produtos.add(produto);			
		}
		
		//imprime os produtos na tela do sistema.
		System.out.println(produtos.toString());
		s.close();
	}
}