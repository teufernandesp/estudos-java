package logica;
import java.util.Scanner;

public class Entrada {

	public static void main(String[] args) {
		
		//recupera os dados na chamada do programa
		//System.out.println(args[0]);

		//interagindo com o usuario.
		Scanner s = new Scanner(System.in);
		System.out.println("Qual seu nome?");
		String nome  =  s.nextLine();
		System.out.println("Bem vindo " + nome);
		s.close();
	}
}