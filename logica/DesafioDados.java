package logica;
import javax.swing.JOptionPane;
import java.util.Random;


public class DesafioDados{

	public static void main(String[] args) {
		
		int aposta = new Integer(JOptionPane.showInputDialog("Informe o numero:"));

		Random dado = new Random();

		int sortDado = dado.nextInt(6) + 1;

		if (sortDado == aposta) {
			
			JOptionPane.showMessageDialog(null, "Voce acertou! ");
			JOptionPane.showMessageDialog(null, aposta + "-" + sortDado);
		}else{

			JOptionPane.showMessageDialog(null, "Voce errou! ");
			JOptionPane.showMessageDialog(null, aposta + "-" + sortDado);
		}
		
		
	}
}